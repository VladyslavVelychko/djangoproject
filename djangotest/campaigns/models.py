from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class Campaign(models.Model):
    TYPE_RETAIL = 0
    TYPE_VENDOR = 1
    TYPE_CHOICES = (
        (TYPE_RETAIL, _("retail")),
        (TYPE_VENDOR, _("vendor")),
    )

    name = models.CharField(
        verbose_name=_("campaign name"),
        max_length=200,
        unique=True) 
    type = models.SmallIntegerField(
        verbose_name=_("campaign type"),
        choices=TYPE_CHOICES,
        default=TYPE_RETAIL)
    domain = models.CharField(
        verbose_name=_("campaign domain"),
        max_length=200,
        db_index=True)
    is_active = models.BooleanField(
        verbose_name=_("is active"),
        default=True,
        db_index=True)
    is_scanning_enabled = models.BooleanField(
        verbose_name=_("is scanning enabled"),
        default=True,
        db_index=True)
    is_url_rewriting_enabled = models.BooleanField(
        verbose_name=_("is URL rewriting enabled"),
        default=True,
        db_index=True)

    class Meta:
        verbose_name = _("Campaign")
        verbose_name_plural = _("Campaigns")
        ordering = ('name',)

    def __unicode__(self):
        return self.name


class Product(models.Model):
    campaign = models.ForeignKey(
        Campaign,
        verbose_name=_("campaign"),
        related_name='products')
    url = models.URLField(
        verbose_name=_("URL"))
    title = models.CharField(
        verbose_name=_("title"),
        max_length=500,
        blank=True,
        db_index=True)
    price = models.FloatField(
        verbose_name=_("price"),
        null=True, blank=True)

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")
        ordering = ('title',)
        unique_together = (
            ('campaign', 'url'),
        )

    def __unicode__(self):
        return self.title or self.url


class Competitor(models.Model):
    campaign = models.ForeignKey(
        Campaign,
        verbose_name=_("campaign"),
        related_name='competitors')
    name = models.CharField(
        verbose_name=_("competitor name"),
        max_length=200,
        db_index=True)
    domain = models.CharField(
        verbose_name=_("competitor domain"),
        max_length=200,
        db_index=True)

    class Meta:
        verbose_name = _("Competitor")
        verbose_name_plural = _("Competitors")
        ordering = ('name',)
        unique_together = (
            ('campaign', 'name'),
        )

    def __unicode__(self):
        return self.name


class Matching(models.Model):
    product = models.ForeignKey(
        Product,
        verbose_name=_("product"),
        related_name='matchings')
    competitor = models.ForeignKey(
        Competitor,
        verbose_name=_("competitor"),
        related_name='matchings')
    url = models.URLField(
        verbose_name=_("URL"),
        db_index=True)
    price = models.FloatField(
        verbose_name=_("price"),
        null=True, blank=True)

    class Meta:
        verbose_name = _("Matching")
        verbose_name_plural = _("Matchings")
        ordering = ('url',)
        unique_together = (
            ('product', 'competitor', 'url'),
        )

    def __unicode__(self):
        return self.url


class Settings(models.Model):
    int_set=models.IntegerField()
    float_set=models.FloatField(null=True, blank=True)
    str_set=models.StringField(max_length=100)
    bool_set=models.BooleanField(default=False)
    json_set=models.CharField(max_length=100)
    user=models.ForeignKey(User)
    RELATIONS={
    ('CUAC':'Concrete user, any company'),
    ('AUCC':'Any user, concrete company'),
    ('AUAC':'Any user, any company'),
    }
    relation=models.CharField(max_length=4,choices=RELATIONS,default='AUAC')
    setting_get=request.GET.get()
    setting_patch=setting_get.patch_list()
